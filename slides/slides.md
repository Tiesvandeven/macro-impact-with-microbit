## Macro impact with the Micro::Bit

---slide---

## Whoami

<img style="float: right;" src="slides/images/photo.jpg">

* Ties van de Ven
* JDriven
* Sharing knowledge
	* Training
	* Coaching

---slide---

<img style="height:800px" src="slides/images/Jinc-logo.png">

---slide---

### Scratch

<img style="height:700px" src="slides/images/scratch.png">

---slide---

### Blocks

<img style="height:700px" src="slides/images/block.png">


---slide---

### Lego mindstorm

<img style="height:800px" src="slides/images/lego.png">

---slide---

## Microbit

<img style="height:500px;float:right;" src="slides/images/microbitheart.jpg">

* Micro:bit Educational Foundation
* "Inspire every child to create their best digital future"
* Combines scratch with hardware
* €15

---slide---

### Specs

<img style="height:500px" src="slides/images/microbit.jpg">

---slide---

### Demo

<img style="height:800px" src="slides/images/ball.png">

https://makecode.microbit.org/_MUH0yhi07Mtz

---slide---

### Other cool projects

---slide---

### Microbike

<img style="height:500px" src="slides/images/microbike.gif">

---slide---

### Plant water system

<img style="height:500px" src="slides/images/plant.png">

---slide---

### Conclusion

* Free course material
	* https://microbit.org/teach/for-teachers/
* Slides
	* https://tiesvandeven.gitlab.io/macro-impact-with-microbit/#/
* My twitter
	* @ties_ven